# Visual machine learning

I will demonstrate some machine learning techniques for visual
analysis of data. It is formulated as exercises to do statistical
validation and, if possible, to extract discriminating genes.

## Exercise

* If you have not yet installed python, follow [these instructions](#install-python)
  to do that.
* Start jupyter and excecute the notebook
  [morten/python/proteomics_I_pca.ipynb](python/proteomics_I_pca.ipynb). (You
  will need to use jupyter to navigate to it. If you press the link
  you see the raw content of the file.)
* See if you understand what happened and why. Ask about the things
  that are unclear. Answer the following questions:
  * Where is the data file that is analyzed located? Open it with
  Excel or another suitable application.
  * What is the shape of the data in the object called `df`? Does it
  change during the excecution?
  * How did we use information about the dosages to learn about the
  distribution of the samples treated with different dosages?
* Leave out one sample in the training stage and find a way to see
  if the python script can be used to predict its dosage.
* Leave out one sample from each dosage group and do the same as
  above for all the samples that have been left out.
* Do the above multiple times and find the probability of the
  prediction beeing correct.
* Find the gene ids with the highest coefficient in the components for
  the PCA. Do they discriminate the samples for different exposures?
  Take a look at sparse PCA and discuss how it can be used to find
  more relevant gene ids.
* Figure out how to translate he gene ids to gene names.
* Execute the remaining notebooks in the morten/python folder.
* Analyze a data set of your own. Are you able to learn the exposures
  of your samples? 
  


### Install python

You will need python 3.7 for these jupyter notebooks. It is the version that comes with Anaconda if you make a fresh install. Otherwise you may need to upgrade python.

#### I reccomend to install python 3 with [Anaconda](https://www.anaconda.com/) for your OS.
     
Once Anaconda is installed, if you are using Windows, open the
Anaconda Prompt and start jupyter by typing `jupyter notebook` at the
prompt. (Typing `jupyter notebook` at the commando line also works.)
If you are using Linux or macOS, open a terminal and start
jupyter by typing `jupyter notebook` at the prompt. 

#### Set up Anaconda

In order to be able to run virtual environments in jupyter notebook a
tweek needs to be done: You need to install
[`nb_conda_kernels`](https://github.com/Anaconda-Platform/nb_conda_kernels). For
linux users, [this seems to be a good
guide](https://www.charles-deledalle.fr/pages/files/configure_conda.pdf)  

I do not know how to install `nb_conda_kernels` in the GUI. Please
inform me if you figure out a way to do that.

### Install git

Follow these [instructions](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) 

### Install dependencies

Navigate to this folder with the terminal or the Anaconda Promt. In the terminal/Promt type

```
pip install .
```

### Test the installation
 
Navigate to the python subfolder of this
repository and see if the notebooks in there works. 
