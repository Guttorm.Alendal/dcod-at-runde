These are the library-size-normalized counts (simply divide the raw counts by the sum of all the counts of the corresponding sample, but since the resulting numbers are too small, so they are then timed by 1000000)

Used R code:
# normalize to library size

args <- commandArgs(TRUE)

file.rawCount <- args[1]
file.normCount <- args[2]

# file.rawCount <- 'ctr1.countmerged.idx'
# file.normCount <- 'ctr1_lib_norm.csv'

data <- read.csv(file.rawCount, sep = '\t', row.names = 1)

num.sample <- ncol(data)
data.norm <- data
lib.size <- rep(0, num.sample)

for (i in c(1:num.sample)){
  lib.size[i] <- sum(data[, i])
  data.norm[, i] <- data.norm[, i] / lib.size[i] * 1e6
}

# data.norm <- data.norm * (sum(lib.size) / num.sample)

write.csv(data.norm, file = file.normCount, quote = F)


