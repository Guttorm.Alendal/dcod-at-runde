
rm(list=ls())  # clearing all variables from the environment
graphics.off() # closing all figures and plots


# setting the working directory with the path to the data file lm.exercise.data.txt
setwd("M:\\pc\\Dokumenter\\Conferences\\Runde Sommer school")     # for MAC user  setwd("/path/to/my/directory")

lm.exercise.data <- read.table("lm.exercise.data.txt", sep="", header=TRUE, row.names=NULL, blank.lines.skip=TRUE, na.strings="NA")

# Here are the variables:
# y is the response variable, it is going to be predicted as a linear combination of the 2 independent variables x and z

print(lm.exercise.data)    # show the whole dataset

print(lm.exercise.data$y)  # show the response variable "y"    -> the one we are interested to predict
print(lm.exercise.data$x)  # show the independent variable "x" -> the one we are interested to predict from
print(lm.exercise.data$z)  # show the independent variable "z" -> the other one we are interested to predict from

# linear regression

a <- lm(y~x,data=lm.exercise.data)
summary(a)

# Call:
#   lm(formula = y ~ x, data = lm.exercise.data)
# 
# Residuals:
#   Min       1Q   Median       3Q      Max 
# -10.4856  -2.9044   0.3155   2.0493  10.6389 
# 
# Coefficients:
#   Estimate Std. Error t value Pr(>|t|)   
# (Intercept)  0.24671    1.57374   0.157  0.87641   
# x            0.08270    0.02758   2.998  0.00522 **
#   ---
#   Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
# 
# Residual standard error: 4.711 on 32 degrees of freedom
# Multiple R-squared:  0.2193,	Adjusted R-squared:  0.1949 
# F-statistic: 8.989 on 1 and 32 DF,  p-value: 0.005219
# 

new_x <- seq(from=-10,to=100,by=5) # new set of x to predict y from

y_predicted <- 0.24671 + 0.08270 * new_x  # y predictions from the new x values (they follow the line (regression line) that is minimizing the difference between observations and predictions)
y_predicted_low_ci <- mean(lm.exercise.data$y) + (0.08270 - 1.96*0.02758) * (new_x-mean(lm.exercise.data$x))  # 95% interval for the regression line (lower boundary)
y_predicted_high_ci <- mean(lm.exercise.data$y) + (0.08270 + 1.96*0.02758) * (new_x-mean(lm.exercise.data$x)) # 95% interval for the regression line (upper boundary)

# plotting results

plot(y~x,data=lm.exercise.data,cex=1.5)
lines(new_x,y_predicted,col="blue",lwd=2)
lines(new_x,y_predicted_low_ci,col="red",lwd=1)
lines(new_x,y_predicted_high_ci,col="red",lwd=1)


########################### Exercise ##############################################3

## Read time-series data
data.cod.n.juv <- read.table("data.cod.n.juv.txt",header=TRUE)
#  logNjuv = log(N) of cod juveniles in Sep-Oct
#  logNlarv = log(N) of cod larvae in June-July and
#  logZoopl = log(biomass) of zooplankton prey in Apr-July
#  header=TRUE the first row in the txt file correspond to the name for the different colums

# We want to analyse the abundance of cod juvenile as a function of larvae abundance and zooplankton biomass
# We want to predict the changes in cod juvenile abundance as a function of a change in larvae abundance and zooplankton biomass

# Accessing the different variables

log.Abundance.juvenile <- data.cod.n.juv$logNjuv   # New.variable "log.Abundance.juvenile" is assigned (<-) the values inside the data frame "data.cod.n.juv" for the column ($) with name "logNjuv"
log.Abundance.larvae   <- data.cod.n.juv$logNlarv  # New.variable "log.Abundance.larvae" is assigned (<-) the values inside the data frame "data.cod.n.juv" for the column ($) with name "logNlarv"
log.biomass.zooplankton<- data.cod.n.juv$logZoopl  # New.variable "log.biomass.zooplankton" is assigned (<-) the values inside the data frame "data.cod.n.juv" for the column( $) with name "logZoopl"





# 1) Look at the distribution of the data, any outliers ?
######################################################




## regression model

## Multiple linear regression model assuming normally distributed errors based on
##   the equation on the logarithmic scale ln(Abundance cod juvenile)= a + b*ln(Abundance cod larvae) + c*ln(Biomass Zooplankton)

## the logarithmic scale equation correspond to the following equation on
##    the "arithmetic" scale (non-logarithmic) Abundance cod juvenile = exp(a) * (Abundance cod larvae) * exp[b * (Abundance cod larvae) + c *(Biomass Zooplankton)]
         


## 2) Fit regression model ?
######################################################




#� 3) Look at the results of the regression, which are the significant variables, which p-value? what is the Rsquare?
######################################################





## 4a) plot the summary of the regression
## 4b) how do the residuals behave? Homoscedasticity? Any residual pattern? Are they normally distributed?
######################################################





## 5) how does the residuals autocorrelation looks like?
######################################################






## What is the change in logNjuv if logNlarv or logZoopl is decreased by 90%?
## in other words What is the change in logNjuv if logNlarv or logZoopl has dropped to 10% of its value?
## To estimate this, we estimate the effect of a change in logNlarv or logZoopl
## of dN = log(1-0.9)  <->   dN = log(0.10)     REMEMBER we are on log scale!

dN <- log(0.1)

## for example  Abundance cod juvenile = exp(a) * (Abundance cod larvae) * exp[b * (Abundance cod larvae) + c *(Biomass Zooplankton)] becomes  Abundance cod juvenile =  exp(a) * (0.1 * Abundance cod larvae) * exp[b * (0.1 * Abundance cod larvae) + c *(Biomass Zooplankton)]
## on the logarithmic scale (as evaluated by the multiple livear regression)  ln(Abundance cod juvenile)= a + b*[ln(0.1) + ln(Abundance cod larvae)] + c*ln(Biomass Zooplankton)
## and thus ln(Abundance cod juvenile)= a + b*ln(0.1) + b*ln(Abundance cod larvae)] + c*ln(Biomass Zooplankton)




## 6) calculate the effect of a change in dN of logNlarv on logNjuv on the log scale
##########################################################





## To compute this we take the value of logNjuv with reduced logNlarv
##    ln(Abundance cod juvenile.red)= a + b*[ln(0.1) + ln(Abundance cod larvae)] + c*ln(Biomass Zooplankton)
## minus the value of logNjuv without reduced logNlarv
##    - ln(Abundance cod juvenile)= - a - ln(Abundance cod larvae)] - c*ln(Biomass Zooplankton)
## thus  ln(Abundance cod juvenile.red) - ln(Abundance cod juvenile) = ln(Abundance cod juvenile.red / Abundance cod juvenile) = b*ln(0.1) 




## 7a) calculate on the non-log scale exp[ln(Abundance cod juvenile.red / Abundance cod juvenile] = ??
## 7b) 7a) is a fraction [0;1]. Convert it into percentage reduction
##########################################################





## Estimates for change on logNjuv with a change in dN of logNlarv or logZoopl with 95 % c.i.:

## 8a) calculate the predicted change on logNjuv with a change in dN in logNlarv on log scale (same as in 6) )
## 8b) calculate the predicted change on logNjuv with a change in dN in logZoopl on log scale
###########################################################





## 9a) calculate the confidence interval (95%) (upper and lower) for the estimate of the effect of logNlarv on logNjuv on the log scale
## 9b) calculate the confidence interval (95%) (upper and lower) for the estimate of the effect of logZoopl on logNjuv on the log scale
###########################################################






## 10a) calculate the predicted change in logNjuv for the lower confidence interval on the estimate of logNlarv on log scale
## 10b) calculate the predicted change in logNjuv for the lower confidence interval on the estimate of logZoopl on log scale
###########################################################






## 11a) calculate the predicted change in logNjuv for the upper confidence interval on the estimate of logNlarv on log scale
## 11b) calculate the predicted change in logNjuv for the upper confidence interval on the estimate of logZoopl on log scale
###########################################################







## 12a) convert 8a) on non-log scale and in percentage reduction
## 12b) convert 8b) on non-log scale and in percentage reduction
###########################################################







## 13a) convert 10a) on non-log scale and in percentage reduction
## 13b) convert 10b) on non-log scale and in percentage reduction
###########################################################







## 14a) convert 11a) on non-log scale and in percentage reduction
## 14b) convert 11b) on non-log scale and in percentage reduction
###########################################################





## Extra Question try to calculate predicted changes in logNjuv for a sequence of dN in logNlarv or logZoopl (from 0.01 to 0.99 for example, by step of 0.02) and the 95% confidence interval
## plot the results
###########################################################

dNs <- seq(from=0.01,to=0.99,by=0.02)
print(dNs)


exp(summary(Model.object)$coefficients["logNlarv","Estimate"]*dNs)*100-100


plot(x,y,lty=1,lwd=2,ylab="Change in abundancce of cod juvenile (%)",xlab="change in abundance of cod larvae (%)")
lines(x,y-c.i.,lty=2,lwd=2)
lines(x,y+c.i.,lty=2,lwd=2)

plot(x,y,lty=1,lwd=2,ylab="Change in abundance of cod juvenile (%)",xlab="change in biomass of zooplankton (%)")
lines(x,y-c.i.,lty=2,lwd=2)
lines(x,y+c.i.,lty=2,lwd=2)

